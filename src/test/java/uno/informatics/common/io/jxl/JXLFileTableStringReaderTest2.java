/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.common.io.jxl;

import java.io.FileNotFoundException;
import java.io.IOException;

import uno.informatics.common.io.TableReader;
import uno.informatics.common.io.TableStringReaderTest;

public class JXLFileTableStringReaderTest2 extends TableStringReaderTest
{
	private static final String FILE = "/string_table.xls";
	
	protected TableReader<String> createReader() throws FileNotFoundException, IOException 
	{
		JXLExcelTableStringReader reader = new JXLExcelTableStringReader(getClass().getResource(FILE).getPath()) ;
		
		reader.setSpreadSheetIndex(1);

		return reader ;
	}
}
