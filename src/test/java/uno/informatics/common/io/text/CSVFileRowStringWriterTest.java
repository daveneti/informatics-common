/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.common.io.text;

import java.io.FileNotFoundException;
import java.io.IOException;

import uno.informatics.common.io.RowReader;
import uno.informatics.common.io.RowStringWriterTest;
import uno.informatics.common.io.RowWriter;
import uno.informatics.common.io.TextFileHandler;

public class CSVFileRowStringWriterTest extends RowStringWriterTest
{
	private static final String FILE = "target/string_table.csv";
	
	protected RowReader<String> createReader() throws FileNotFoundException, IOException 
	{
		TextFileRowStringReader reader = new TextFileRowStringReader(FILE) ;
		
		reader.setDelimiterString(TextFileHandler.COMMA);
		
		return reader ;
	}
	
	/* (non-Javadoc)
	 * @see uno.informatics.common.io.RowWriterTest#createWriter()
	 */
  @Override
  protected RowWriter<String> createWriter() throws FileNotFoundException,
      IOException
  {
		TextFileRowStringWriter writer = new TextFileRowStringWriter(FILE) ;
		
		writer.setDelimiterString(TextFileHandler.COMMA);
		
		return writer ;
  }
}
