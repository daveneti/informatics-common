/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.common.io.text;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import uno.informatics.common.io.TableObjectWriterTest;
import uno.informatics.common.io.TableReader;
import uno.informatics.common.io.TableWriter;
import uno.informatics.common.io.TextFileHandler;

public class CSVFileStreamTableObjectWriterTest extends TableObjectWriterTest
{
	private static final String FILE = "target/object_table.csv";
	
	protected TableReader<Object> createReader() throws FileNotFoundException, IOException 
	{
		TextFileTableObjectReader reader = new TextFileTableObjectReader(FILE) ;
		
		reader.setDelimiterString(TextFileHandler.COMMA);
		
		return reader ;
	}
	
	/* (non-Javadoc)
	 * @see uno.informatics.common.io.RowWriterTest#createWriter()
	 */
  @Override
  protected TableWriter<Object> createWriter() throws FileNotFoundException, IOException
  {
		TextFileTableObjectWriter writer = 
				new TextFileTableObjectWriter(new BufferedWriter(new FileWriter(FILE))) ;
		
		writer.setDelimiterString(TextFileHandler.COMMA);
		
		return writer ;
  }
}
