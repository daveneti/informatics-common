/*******************************************************************************
 * Copyright 2010 Guy Davenport Licensed under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law
 * or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 *******************************************************************************/
package uno.informatics.common;

/**
 * @author Guy Davenport
 *
 */
public interface DataTypeConstants
{

	public static final int	UNKNOWN	      = 0;
	public static final int	BOOLEAN	      = 1 << 0;
	public static final int	SHORT	        = 1 << 1;
	public static final int	INT	          = 1 << 2;
	public static final int	LONG	        = 1 << 3;
	public static final int	FLOAT	        = 1 << 4;
	public static final int	DOUBLE	      = 1 << 5;
	public static final int	BIG_INTEGER	  = 1 << 6;
	public static final int	BIG_DECIMAL	  = 1 << 7;
	public static final int	LSID	        = 1 << 8;
	public static final int	DATE	        = 1 << 9;
	public static final int	STRING	      = 1 << 10;
	public static final int	DEFAULT_TYPES	= BOOLEAN | INT | DOUBLE | STRING | DATE ;
	public static final int	NUMBER	      = SHORT | INT | LONG | FLOAT | DOUBLE
	                                          | BIG_INTEGER | BIG_DECIMAL;
	public static final int	INTEGER	      = SHORT | INT | LONG | BIG_INTEGER;
	public static final int	REAL	        = FLOAT | DOUBLE | BIG_DECIMAL;
	public static final int	BIG_NUMBER	  = BIG_INTEGER | BIG_DECIMAL;

}