/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.common.io.text;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import uno.informatics.common.io.CellWriter;
import uno.informatics.common.io.RowAndCellWriter;
import uno.informatics.common.io.RowWriter;

public abstract class AbstractTextFileRowWriter<T extends Object> extends AbstractTextFileWriter<T> implements RowAndCellWriter<T>
{
	public AbstractTextFileRowWriter(String file) throws FileNotFoundException, IOException
	{
		super(new File(file)) ;
	}

	public AbstractTextFileRowWriter(File file) throws FileNotFoundException, IOException
	{
		super(file) ;
	}
	
	public AbstractTextFileRowWriter(BufferedWriter bufferedWriter) throws FileNotFoundException, IOException
	{
		super(bufferedWriter) ;
	}

	@Override
  public final boolean newRow() throws IOException
  {
	  return super.newRow() ;
  }
	
	@Override
	public final boolean newColumn() throws IOException
  {
	  return super.newColumn();
  }

	@Override
	public final void writeRowCellsAsArray(T[] cells) throws IOException
  {
	  super.writeRowCellsAsArray(cells);
  }

	@Override
	public final void writeRowCells(List<T> cells) throws IOException
  {
	  super.writeRowCells(cells);
  }
	
	@Override
	public final void writeCell(T cell) throws IOException
  {
	  super.writeCell(cell);
  }
}
