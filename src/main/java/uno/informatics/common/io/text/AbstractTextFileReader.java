/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.common.io.text;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.regex.Pattern;

import uno.informatics.common.Constants;
import uno.informatics.common.ConversionUtilities;
import uno.informatics.common.DataTypeConstants;

public abstract class AbstractTextFileReader<T extends Object> extends AbstractTextFileHandler<T>
{    
  private boolean parsingEmptyStrings ;
  
  private boolean convertingValues ;

  private Map<Integer, Integer> conversionTypesMap  ;

  private int conversionTypesCount;

  private int[] conversionTypesArray;

	private int defaultConversionTypes;
  
  private Pattern pattern ;
  
  private BufferedReader bufferedReader;
  
  private boolean ignoringMultipleDelimiters ;

  private List<T> row ;
  
  private String line;
	private String nextLine;
	
	@SuppressWarnings("unused")
  private int firstRow;

	@SuppressWarnings("unused")
  private int firstColumn;

	private T[] rowArray;

  private static final String BUFFERREADER_NULL = "Buffer reader is undefined";
  
	private AbstractTextFileReader(int firstRow, int firstColumn)
  {
	  this.firstRow = firstRow ;
	  this.firstColumn = firstColumn ;
	  
	  conversionTypesArray = new int[0] ;
  }
	
  /**
   * Constructs an initialised reader using a string reference to a text file.
   * 
   * @param reference
   *          a text file name or URL
   * @throws IOException 
   * @throws FileNotFoundException 
   */
  public AbstractTextFileReader(String reference) throws IOException, FileNotFoundException
  {
    this(reference, 0, 0) ;
  }

  /**
   * Constructs an initialised reader using a file.
   * 
   * @param file
   *          a text File object.
   * @throws IOException 
   * @throws FileNotFoundException 
   */
  public AbstractTextFileReader(File file) throws IOException, FileNotFoundException
  {
    this(file, 0, 0) ;
  }
  
	public AbstractTextFileReader(BufferedReader bufferedReader) throws IOException
  {
    this(bufferedReader, 0, 0) ;
  }

  /**
   * Constructs an initialised reader using a string reference to a text file.
   * 
   * @param reference
   *          a text file name or URL
   * @throws IOException 
   * @throws FileNotFoundException 
   */
  public AbstractTextFileReader(String reference, int firstRow, int firstColumn) throws IOException, FileNotFoundException
  {
  	this(firstRow, firstColumn) ;
  	
    if (reference == null)
      throw new FileNotFoundException("File undefined");

    setFileReference(reference) ;
    
    initialise() ;
  }

  /**
   * Constructs an initialised reader using a file.
   * 
   * @param file
   *          a text File object.
   * @throws IOException 
   * @throws FileNotFoundException 
   */
  public AbstractTextFileReader(File file, int firstRow, int firstColumn) throws IOException, FileNotFoundException
  {
  	this(firstRow, firstColumn) ;
  	
    if (file == null)
      throw new FileNotFoundException("File undefined");

    setFile(file) ;
    
    initialise() ;
  }
  
	public AbstractTextFileReader(BufferedReader bufferedReader, int firstRow, int firstColumn) throws IOException
  {
  	this(firstRow, firstColumn) ;
  	
    if (bufferedReader != null)
    	this.bufferedReader = bufferedReader ;
    else
      throw new IOException("Buffered reader undefined");
    
    initialise() ;
  }

	public final boolean ready()
  {
    try
    {
      if (bufferedReader != null)
        return bufferedReader.ready() ;
      else
        return false ;
    }
    catch (IOException e)
    {
      return false ;
    }
  }

  public final void close() 
  {
    try
    {
      if (bufferedReader != null)
        bufferedReader.close();
    }
    catch (IOException e)
    {

    }
    
    bufferedReader = null ;
  }

  /**
   * @return the ignoringMultipleDelimiters
   */
  public final boolean isIgnoringMultipleDelimiters()
  {
    return ignoringMultipleDelimiters;
  }

  /**
   * @param ignoringMultipleDelimiters the ignoringMultipleDelimiters to set
   * @throws MatrixException 
   */
  public final void setIgnoringMultipleDelimiters(
      boolean ignoringMultipleDelimiters) throws IOException
  {
    if (ignoringMultipleDelimiters != this.ignoringMultipleDelimiters)
    {
      if (isInUse())
        throw new IOException("Paramater can not be changed while reader is in use") ;
      
      this.ignoringMultipleDelimiters = ignoringMultipleDelimiters ;
    }
  }
  
  /**
   * @return the convertingValues
   */
  public final boolean isConvertingValues()
  {
    return convertingValues;
  }

  /**
   * @param convertingValues the convertingValues to set
   * @throws IOException 
   */
  public final void setConvertingValues(boolean convertingValues) throws IOException
  {
    if (convertingValues != this.convertingValues)
    {
      if (isInUse())
        throw new IOException("Paramater can not be changed while reader is in use") ;
      
      this.convertingValues = convertingValues ;
    }
  }

  public final boolean isParsingEmptyStrings()
  {
    return parsingEmptyStrings;
  }

  public final void setParsingEmptyStrings(boolean parsingEmptyStrings) throws IOException
  {
    if (this.parsingEmptyStrings != parsingEmptyStrings)
    {
      if (isInUse())
        throw new IOException("Paramater can not be changed while reader is in use") ;
      
      this.parsingEmptyStrings = parsingEmptyStrings;
      
      updatePattern() ;
    }
  }
  
  public final synchronized void setDelimiterString(String delimiter) throws IOException
  {
    super.setDelimiterString(delimiter) ;
    
    updatePattern();
  }
  
  protected T[][] readCellsAsArray() throws IOException
	{
  	List<List<T>> cells = readCells() ;
  	
  	T[][] array = createArray(cells.size(), getRowSize()) ;
  	
  	Iterator<List<T>> iterator = cells.iterator() ;
  	List<T> row ;
  	
  	int i = 0 ;
  	
  	while (iterator.hasNext() && i < cells.size())
  	{
  		row = iterator.next() ;
  		array[i] = row.toArray(array[i]) ;
  		
  		++i ;
  	}
  	
  	return array ;
	}
  
  protected List<List<T>> readCells() throws IOException
  {
  	List<List<T>> cells = new LinkedList<List<T>>() ;
  	
	  while (nextRow())
	  {
	  	cells.add(getRowCells()) ;
	  }
  	
  	return cells ;
  }
  
	protected boolean hasNextRow()
  {
    return nextLine != null ;
  }

	protected boolean nextRow() throws IOException
  {
    if (hasNextRow())
    {
    	readNextLine() ;
    	incrementRowIndex(); 
    	
    	return true ;
    }
    else
    {
    	return false ;
    }
  }
	
	protected List<T> getRowCells() throws IOException
  {
		if (row == null)
			parseRowCells() ;
		
		return row ;
  }

	protected T[] getRowCellsAsArray() throws IOException
  {
		if (rowArray == null)
			parseRowArray() ;
		
		return rowArray ;
  }

	protected abstract T[] createArray(int size) ;
	
	protected abstract T[][] createArray(int rowCount, int columnCount) ;


  @SuppressWarnings("unchecked")
  protected T parseValue(String text, int rowIndex, int columnIndex) throws IOException 
  {
	  try
    {
	    return (T)convertValue(text);
    }
    catch (ClassCastException e)
    {
	    throw new IOException(e.getLocalizedMessage(), e) ;
    }
  }
  
  @SuppressWarnings("unchecked")
  protected T parseValue(String text, int rowIndex, int columnIndex, int conversionTypes) throws IOException 
  {
	  try
    {
	    return (T)convertValue(text, conversionTypes);
    }
    catch (ClassCastException e)
    {
	    throw new IOException(e.getLocalizedMessage(), e) ;
    }
  }
  
  protected Object convertValue(String text) 
  {
    return ConversionUtilities.convertToObject(text) ;
  }
  
  protected Object convertValue(String text, int dataTypes) 
  {
    return ConversionUtilities.convertToObject(text, dataTypes) ;
  }

  /**
   * Initialises the reader
   * @throws IOException 
   * @throws FileNotFoundException 
   */
  protected final void initialise() throws FileNotFoundException, IOException 
  {
  	super.initialise();
  	
    defaultConversionTypes = DataTypeConstants.DEFAULT_TYPES ;

    conversionTypesMap = new TreeMap<Integer, Integer>() ;
    
    conversionTypesCount = 0 ;
    
    conversionTypesArray = null ;
    
    updatePattern() ;

    if (getFileReference() != null)
      initialiseBufferedReader(getBufferReader(getFileReference()));
    else
      if (getFile() != null)
        initialiseBufferedReader(getBufferReader(getFile()));
      else
        if (bufferedReader != null)
          initialiseBufferedReader(bufferedReader) ;
        else
          throw new IOException("Unable to initialise reader") ;
    
    readNextLine() ; // cache first line
    
    // no rows!
    if (nextLine == null)
    	updateRowSize(0) ;
  }
  
  private void readNextLine() throws IOException
  {
  	row = null ;
  	rowArray = null ;
  	line = nextLine ;
  	nextLine = null ;
    setCellPosition(0) ;
    
    while (bufferedReader.ready() && nextLine == null)
    {
    	nextLine = bufferedReader.readLine();
    
      //    ignore any commented record or empty lines if not in strict mode
      if (nextLine != null && ((nextLine.trim().length() == 0 && isInStrictMode())
          || (getCommentString() != null && nextLine.trim().startsWith(getCommentString()))))
      {
      	nextLine = null ;
      }
      
      incrementRowPosition() ;
    }
  }

	/**
   * Initialises the reader using a bufferedReader directly.
   * 
   * @param bufferedReader
   *          a buffered reader
   */
  private final void initialiseBufferedReader(BufferedReader bufferedReader)
  {
    if (bufferedReader == null)
      throw new NullPointerException(BUFFERREADER_NULL);
    
    this.bufferedReader = bufferedReader;
    
    setRowPosition(0) ;
    setRowSizeInternal(Constants.UNKNOWN_ROW_COUNT) ;
    setRowIndex(Constants.UNKNOWN_ROW_INDEX) ;
  }
  
  private final void updatePattern()
  {
    // TODO need to check for special characters
    if (ignoringMultipleDelimiters)
      pattern = Pattern.compile(getDelimiterString() + "+", Pattern.DOTALL) ;
    else
      pattern = Pattern.compile(getDelimiterString(), Pattern.DOTALL) ;
  }

  protected String convertToken(String string)
  {
    if (isParsingEmptyStrings())
      return string ;
    else
      if (string != null)
        if ("".equals(string.trim()))
          return null ;
        else
          return string ;
      else
        return null ;
  }

  protected int getDefaultConversionTypes()
  {
    // TODO Auto-generated method stub
    return defaultConversionTypes ;
  }

  public final void setDefaultConversionTypes(int defaultConversionTypes)
  {
    this.defaultConversionTypes = defaultConversionTypes ;
  }
  
  public final int getConversionTypes(int index)
  {
    if (index >= 0 && conversionTypesMap.containsKey(index))
      return conversionTypesMap.get(index);
    else
      return defaultConversionTypes ;
  }
  
  public final int[] getAllConversionTypes()
  {
    if (conversionTypesArray == null)
    {
      conversionTypesArray = new int[conversionTypesCount] ;
      
      Iterator<Entry<Integer, Integer>> iterator = conversionTypesMap.entrySet().iterator() ;
      
      Entry<Integer, Integer> entry = null ;
      
      while (iterator.hasNext())
      {
        entry = iterator.next() ;
        
        conversionTypesArray[entry.getKey()] = entry.getValue() ;
      }
    }
    
    return conversionTypesArray ;
  }

  public final void setAllConversionTypes(int[] conversionTypes)
  {
    conversionTypesArray = null ;
    conversionTypesCount = conversionTypes.length ;
    for (int i = 0 ; i < conversionTypes.length ; ++i)
      conversionTypesMap.put(i, conversionTypes[i]) ;
  }

  public final  void setConversionTypes(int conversionTypes, int index)
  {
    if (index >= 0)
    {
      conversionTypesMap.put(index, conversionTypes) ;
      
      if (index >= conversionTypesCount)
      {
        conversionTypesCount = index + 1 ;
        conversionTypesArray = null ;
      }
      else
      {
        conversionTypesArray[index] = conversionTypes ;
      }
    }
  }
  
  private void parseRowCells() throws IOException
  {
		if (line != null)
		{
			String[] tokens = pattern.split(line) ;
			
			row = new ArrayList<T>(tokens.length) ;
			
			updateRowSize(tokens.length) ;
			
			int i = 0 ;

			if (conversionTypesCount > 0)
			{
				while (i < tokens.length)
				{
					row.add(parseValue(convertToken(tokens[i]), getRowIndex(), i, getConversionTypes(i))) ;
					++i ;
				}
			}
			else
			{
				while (i < tokens.length)
				{
					row.add(parseValue(convertToken(tokens[i]), getRowIndex(), i)) ;
					++i ;
				}	
			}
		}
		else
		{
			row = new ArrayList<T>() ;
		}

		updateRowFromSize(row) ;
  }
  
	private void parseRowArray() throws IOException
  {
		if (line != null)
		{
			String[] tokens = pattern.split(line) ;
			
			updateRowSize(tokens.length) ;
			
			rowArray = createArray(getRowSize()) ;

			for (int i = 0 ; i < tokens.length ; ++i)
			{
				rowArray[i] = parseValue(convertToken(tokens[i]), getRowIndex(), i) ;
			}
		}

		updateRowFromSize(row) ;
  }

  /**
   * Creates a BufferedReader using the string reference to a text file.
   * 
   * @param textFileReference
   *          a text file name or URL
   * @return a bufferedReader
   * 
   * @exception FileNotFoundException
   *              if the file referenced can not be found
   * @exception IOException
   *              if the reader can not open an input stream to the file
   */
  private static final BufferedReader getBufferReader(File file)
      throws FileNotFoundException, IOException
  {
    if (file != null)
      return new BufferedReader(new FileReader(file));
    else
      throw new FileNotFoundException("File object is null");
  }

  /**
   * Creates a BufferedReader using the string reference to a text file.
   * 
   * @param textFileReference
   *          a text file name or URL
   * @return a bufferedReader
   * 
   * @exception FileNotFoundException
   *              if the file referenced can not be found
   * @exception IOException
   *              if the reader can not open an input stream to the file
   */
  private static final BufferedReader getBufferReader(String fileReference)
      throws FileNotFoundException, IOException
  {
    BufferedReader bufferedReader = null;

    try
    {
      URL refURL = new java.net.URL(fileReference);
      bufferedReader = new BufferedReader(new InputStreamReader(refURL
          .openStream()));
    }
    catch (MalformedURLException malformedURLException)
    {
      bufferedReader = new BufferedReader(new FileReader(fileReference));
    }

    return bufferedReader;
  }
}
