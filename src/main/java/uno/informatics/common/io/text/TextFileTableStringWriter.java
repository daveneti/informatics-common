/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.common.io.text;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TextFileTableStringWriter extends AbstractTextFileTableWriter<String>
{
	public TextFileTableStringWriter(String file) throws FileNotFoundException, IOException
	{
		super(new File(file)) ;
	}

	public TextFileTableStringWriter(File file) throws FileNotFoundException, IOException
	{
		super(file) ;
	}
	
	public TextFileTableStringWriter(BufferedWriter bufferedWriter)
      throws FileNotFoundException, IOException
  {
	  super(bufferedWriter);
  }

	/* (non-Javadoc)
	 * @see uno.informatics.common.io.text.AbstractTextFileWriter#convertValue(java.lang.Object)
	 */
  @Override
  protected final String convertValue(String value)
  {
	  return value;
  }
}
