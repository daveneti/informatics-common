/*******************************************************************************
 * Copyright 2010 Guy Davenport
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uno.informatics.common;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ConversionUtilities implements DataTypeConstants
{
	private static final String TRUE = "true";
	private static final String FALSE = "false";
	
	private static final DateFormat DATE_FORMAT = DateFormat.getDateInstance();
	
  @SuppressWarnings("unused")
  private static String DEFAULT_DELIMITER    = "&" ;

  public static final String convertToString(String value)
  {
    return value;
  }

  /**
   * Converts a string to an object. Will try to convert in following order
   * Integer, Double and Boolean, or return the original String
   * 
   * @param string the string to be converted
   * @return a object representation of the string
   */
  public static final Object convertToObject(String string)
  {
    return convertToObject(string, DEFAULT_TYPES);
  }

  /**
   * Converts a string to an object given a set of possible types to convert to. Will try to convert in following order
   * Short, Integer, Long, Float, Double, BigInteger, BigDecmal and Boolean, or return the original String
   * 
   * @param string the string to be converted
   * @param types types to convert
   * @return a object representation of the string
   */
  public static final Object convertToObject(String string, int types)
  {
    Object value = null ;
    
    if (string != null)
    {      
      if (value == null && (types & SHORT) != 0)
        value = convertToShortInternal(string) ;
      
      if (value == null && (types & INT) != 0)
        value = convertToIntegerInternal(string) ;
      
      if (value == null && (types & LONG) != 0)
        value = convertToLongInternal(string) ;
      
      if (value == null && (types & FLOAT) != 0)
        value = convertToFloatInternal(string) ;
      
      if (value == null && (types & DOUBLE) != 0)
        value = convertToDoubleInternal(string) ;
      
      if (value == null && (types & BIG_INTEGER) != 0)
        value = convertToBigIntegerInternal(string) ;

      if (value == null && (types & BIG_DECIMAL) != 0)
        value = convertToBigDecimalInternal(string) ;
      
      if (value == null && (types & BOOLEAN) != 0)
        value = convertToBooleanInternal(string) ;
      
      if (value == null && (types & DATE) != 0)
        value = convertToDateInternal(string) ;
      
      if (value == null && (types & LSID) != 0)
        value = convertToLSIDInternal(string) ;
      
      if (value == null && (types & STRING) != 0)
        value = string ;
    }
    
    return value;
  }


  public static final BigDecimal convertToBigDecimal(String string) throws ConversionException
  {
    BigDecimal value = null ;
    
    if (string != null)
    {
      try
      {
        value = new BigDecimal(string) ;
      }
      catch (NumberFormatException e)
      {
        throw new ConversionException(e.getMessage(), e);
      }
    }
    
    return value ;
  }
  
  public static final BigInteger convertToBigInteger(String string) throws ConversionException
  {
    BigInteger value = null ;
    
    if (string != null)
    {
      try
      {
        value = new BigInteger(string) ;
      }
      catch (NumberFormatException e)
      {
        throw new ConversionException(e.getMessage(), e);
      }
    }
    
    return value ;
  }
  
  public static final Double convertToDouble(String string) throws ConversionException
  {
    Double value = null ;
    
    if (string != null)
    {
      try
      {
        value = Double.valueOf(string);
      }
      catch (NumberFormatException e)
      {
        throw new ConversionException(e.getMessage(), e);
      }
    }
    
    return value ;
  }

  public static final Float convertToFloat(String string) throws ConversionException
  {
    Float value = null ;
    
    if (string != null)
    {
      try
      {
        value = Float.valueOf(string);
      }
      catch (NumberFormatException e)
      {
        throw new ConversionException(e.getMessage(), e);
      }
    }
    
    return value ;
  }
  
  public static final Long convertToLong(String string) throws ConversionException
  {
    Long value = null ;
    
    if (string != null)
    {
      try
      {
        value = Long.valueOf(string);
      }
      catch (NumberFormatException e)
      {
        throw new ConversionException(e.getMessage(), e);
      }
    }
    
    return value ;
  }
  public static final Integer convertToInteger(String string) throws ConversionException
  {
    Integer value = null ;
    
    if (string != null)
    {
      try
      {
        value = Integer.valueOf(string);
      }
      catch (NumberFormatException e)
      {
        throw new ConversionException(e.getMessage(), e);
      }
    }
    
    return value ;
  }
  
  public static final Short convertToShort(String string) throws ConversionException
  {
    Short value = null ;
    
    if (string != null)
    {
      try
      {
        value = Short.valueOf(string);
      }
      catch (NumberFormatException e)
      {
        throw new ConversionException(e.getMessage(), e);
      }
    }
    
    return value ;
  }
  
  public static final Boolean convertToBoolean(String string) throws ConversionException
  {
  	Boolean value = null ;
    
    if (string != null)
    {
    	if (TRUE.equals(string.toLowerCase()))
    	{
    		return true ;
    	}
    	else
      {
      	if (FALSE.equals(string.toLowerCase()))
      	{
      		return false ;
      	}
      }
    }
    
    return value ;
  }
  
  @SuppressWarnings("deprecation")
  public static final Date convertToDate(String value) throws ConversionException
  {
    if (value != null)
    {
	    try
      {
	      return DATE_FORMAT.parse(value) ;
      }
      catch (Exception e)
      {
  	    try
        {
  	      return new Date(value) ;
        }
        catch (Exception e2)
        {
  	      throw new ConversionException(e2) ;
        }
      }
    }
    else
    {
      return null ;
    }
  }
  
  public static final LSID convertToLSID(String value) throws ConversionException
  {
    if (value != null && uno.informatics.common.LSID.isLSID(value))
      return new LSID(value) ;
    else
      return null ;
  }

  public static final String convertToString(Object value)
  {
    if (value != null)
    	if (value instanceof Date)
    		return DATE_FORMAT.format((Date)value) ;
    	else
    		return value.toString() ;
    else
      return null ;
  }

  public static final Double convertToDouble(Object value) throws ConversionException
  {
    if (value != null)
    {
      if (value instanceof Double)
      {
        return (Double)value ;
      }
      else
      {
        if (value instanceof Number)
        {
          return ((Number)value).doubleValue() ;
        }
        else
        {
          if (value instanceof String)
          {
            return convertToDouble((String)value) ;
          }
          else
          {
            throw new ConversionException("Can not convert value to double : " + value) ;
          }
        }
      }
    }
    else
    {
      return null ;
    }  
  }
  
  public static final Integer convertToInteger(Object value) throws ConversionException
  {
    if (value != null)
    {
      if (value instanceof Integer)
      {
        return (Integer)value ;
      }
      else
      {
        if (value instanceof Number)
        {
          return ((Number)value).intValue() ;
        }
        else
        {
          if (value instanceof String)
          {
            return convertToInteger((String)value) ;
          }
          else
          {
            throw new ConversionException("Can not convert value to integer : " + value) ;
          }
        }
      }
    }
    else
    {
      return null ;
    }  
  }
  
	/**
	 * @param cells
	 * @param types
	 * @return
	 */
  public static List<Object> convertToObjectList(List<String> cells, int[] types)
  {
  	List<Object> list = new ArrayList<Object>() ;

  	Iterator<String> iterator = cells.iterator() ;
  	
  	int i = 0 ;
  	
  	while(iterator.hasNext())
  	{
  		list.add(convertToObject(iterator.next(), types[i])) ;
  		
  		++i ;
  	}
  	
	  return list;
  }
  
  public static final int getDataType(String string)
  {
    int datatype = UNKNOWN ;
    
    if (string != null)
    {   
    	datatype = STRING ;
    	
      if (convertToShortInternal(string) != null)
      	datatype = datatype | SHORT ;
      
      if (convertToIntegerInternal(string)!= null)
      	datatype = datatype | INT ;
      
      if (convertToLongInternal(string)!= null)
      	datatype = datatype | LONG ;
      
      if (convertToFloatInternal(string) != null)
      	datatype = datatype | FLOAT ;
      
      if (convertToDoubleInternal(string)!= null)
      	datatype = datatype | DOUBLE ;
      
      if (convertToBigIntegerInternal(string)!= null)
      	datatype = datatype | BIG_INTEGER ;

      if (convertToBigDecimalInternal(string) != null)
      	datatype = datatype | BIG_DECIMAL ;
      
      if (convertToBooleanInternal(string)!= null)
      	datatype = datatype | BOOLEAN ;
      
      if (convertToDateInternal(string)!= null)
      	datatype = datatype | DATE ;
      
      if (convertToLSIDInternal(string)!= null)
      	datatype = datatype | LSID ;
    }
    
    return datatype ;
  }
  
  public static final List<Integer> getDataTypes(List<String> strings)
  {
  	List<Integer> datatypes ;

    if (strings != null)
    {      
    	datatypes = new ArrayList<Integer>(strings.size()) ;
    	
    	Iterator<String> iterator = strings.iterator() ;
    	
      while(iterator.hasNext())
      	datatypes.add(getDataType(iterator.next())) ;
    }
    else
    {
    	datatypes = new ArrayList<Integer>() ;
    }
    
    return datatypes ;
  }
  
  public static final int[] getDataTypes(String[] strings)
  {
  	int[] datatypes ;

    if (strings != null)
    {      
    	datatypes = new int[strings.length];
    	  	
      for(int i = 0 ; i < strings.length ; ++i)
      	datatypes[i] = getDataType(strings[i]) ;
    }
    else
    {
    	datatypes = new int[0];
    }
    
    return datatypes ;
  }
  
  public static final List<Integer> getDataTypes(List<String> strings, List<Integer> currentDatatypes)
  {
  	List<Integer> datatypes ;

    if (strings != null && currentDatatypes != null && strings.size() == currentDatatypes.size())
    {      
    	datatypes = new ArrayList<Integer>(strings.size()) ;
    	
    	Iterator<String> iterator = strings.iterator() ;
    	Iterator<Integer> iterator2 = currentDatatypes.iterator() ;
    	
      while(iterator.hasNext() && iterator2.hasNext())
      	datatypes.add(iterator2.next() & getDataType(iterator.next())) ;
    }
    else
    {
    	datatypes = new ArrayList<Integer>(currentDatatypes) ;
    }
    
    return datatypes ;
  }
  
  public static final int[] getDataTypes(String[] strings, int[] currentDatatypes)
  {
  	int[] datatypes ;

    if (strings != null && currentDatatypes != null && strings.length == currentDatatypes.length)
    {      
    	datatypes = new int[strings.length];
    	  	
      for(int i = 0 ; i < strings.length ; ++i)
      	datatypes[i] = currentDatatypes[i] & getDataType(strings[i]) ;
    }
    else
    {
    	datatypes = currentDatatypes ;
    }

    return datatypes ;
  }

  private static final BigDecimal convertToBigDecimalInternal(String string)
  {
    try
    {
      return convertToBigDecimal(string);
    }
    catch (ConversionException e)
    {
      return null ;
    }
  }

  private static final BigInteger convertToBigIntegerInternal(String string)
  {
    try
    {
      return convertToBigInteger(string);
    }
    catch (ConversionException e)
    {
      return null ;
    }
  }

  private static final Double convertToDoubleInternal(String string)
  {
    try
    {
      return convertToDouble(string);
    }
    catch (ConversionException e)
    {
      return null ;
    }
  }

  private static final Float convertToFloatInternal(String string)
  {
    try
    {
      return convertToFloat(string);
    }
    catch (ConversionException e)
    {
      return null ;
    }
  }

  private static final Long convertToLongInternal(String string)
  {
    try
    {
      return convertToLong(string);
    }
    catch (ConversionException e)
    {
      return null ;
    }
  }

  private static final Integer convertToIntegerInternal(String string)
  {
    try
    {
      return convertToInteger(string);
    }
    catch (ConversionException e)
    {
      return null ;
    }
  }

  private static final Short convertToShortInternal(String string)
  {
    try
    {
      return convertToShort(string);
    }
    catch (ConversionException e)
    {
      return null ;
    }
  }
  
  private static final Boolean convertToBooleanInternal(String string)
  {
    try
    {
      return convertToBoolean(string);
    }
    catch (ConversionException e)
    {
      return null ;
    }
  }
  
  private static final Date convertToDateInternal(String string)
  {
    try
    {
      return convertToDate(string);
    }
    catch (ConversionException e)
    {
      return null ;
    }
  }
  
  private static final LSID convertToLSIDInternal(String string)
  {
    try
    {
      return convertToLSID(string);
    }
    catch (ConversionException e)
    {
      return null ;
    }
  }
}
